highlight clear 

set background=light

hi clear
if exists('syntax_on')
  syntax reset
endif

highlight! Normal guibg=#ffffea guifg=#000000 ctermbg=230 ctermfg=232 
highlight! Keyword guibg=#ffffea guifg=#000000 gui=bold ctermbg=230 ctermfg=232 cterm=bold
highlight! Function guibg=#ffffea guifg=#000000 gui=bold ctermbg=230 ctermfg=232 cterm=bold
highlight! Type guibg=#ffffea guifg=#000000 gui=bold ctermbg=230 ctermfg=232 cterm=bold
highlight! NonText guibg=bg guifg=#ffffea ctermbg=bg ctermfg=230
highlight! StatusLine guibg=#aeeeee guifg=#000000 gui=NONE ctermbg=159 ctermfg=232 cterm=NONE
highlight! StatusLineNC guibg=#eaffff guifg=#000000 gui=NONE ctermbg=194 ctermfg=232 cterm=NONE
highlight! WildMenu guibg=#000000 guifg=#eaffff gui=NONE ctermbg=black ctermfg=159 cterm=NONE
highlight! VertSplit guibg=#ffffea guifg=#000000 gui=NONE ctermbg=159 ctermfg=232 cterm=NONE
highlight! Folded guibg=#cccc7c guifg=fg gui=NONE ctermbg=187 ctermfg=fg cterm=NONE
highlight! FoldColumn guibg=#fcfcce guifg=fg ctermbg=229 ctermfg=fg
highlight! Conceal guibg=bg guifg=fg gui=NONE ctermbg=bg ctermfg=fg cterm=NONE
highlight! LineNr guibg=bg guifg=#505050 gui=NONE ctermbg=bg ctermfg=239 cterm=NONE
highlight! Visual guibg=fg guifg=bg ctermbg=fg ctermfg=bg
highlight! CursorLine guibg=#ffffca guifg=fg ctermbg=230 ctermfg=fg
highlight! Pmenu guibg=bg guifg=#800000 ctermbg=bg ctermfg=1
highlight! PmenuSel guibg=fg guifg=bg ctermbg=fg ctermfg=bg

highlight! Statement guibg=bg guifg=fg gui=NONE ctermbg=bg ctermfg=fg cterm=NONE
highlight! Identifier guibg=bg guifg=fg gui=NONE ctermbg=bg ctermfg=fg cterm=NONE
highlight! Type guibg=bg guifg=fg gui=NONE ctermbg=bg ctermfg=fg cterm=NONE
highlight! PreProc guibg=bg guifg=fg gui=NONE ctermbg=bg ctermfg=fg cterm=NONE
highlight! Constant guibg=bg guifg=#101010 gui=NONE ctermbg=bg ctermfg=233 cterm=NONE
highlight! Comment guibg=bg guifg=grey gui=NONE ctermbg=bg ctermfg=grey cterm=NONE
highlight! String guibg=bg guifg=grey gui=NONE ctermbg=bg ctermfg=grey cterm=NONE
highlight! Special guibg=bg guifg=fg gui=NONE ctermbg=bg ctermfg=fg cterm=NONE
highlight! SpecialKey guibg=bg guifg=fg gui=bold ctermbg=bg ctermfg=fg cterm=bold
highlight! Directory guibg=bg guifg=fg gui=bold ctermbg=bg ctermfg=fg cterm=bold
highlight! link Title Directory
highlight! link MoreMsg Comment
highlight! link Question Comment

hi TabLine ctermfg=0 ctermbg=7 guifg=#000000 guibg=#f3f3d3 guisp=NONE cterm=NONE gui=NONE
hi TabLineFill ctermfg=0 ctermbg=7 guifg=#000000 guibg=#f3f3d3 guisp=NONE cterm=NONE gui=NONE
hi TabLineSel ctermfg=0 ctermbg=12 guifg=#000000 guibg=#ebffff guisp=NONE cterm=NONE gui=NONE

hi! link ColorColumn CursorLine
hi! link CursorColumn CursorLine
hi! link ErrorMsg Error
hi! link SignColumn Normal
hi! link vimAutoCmdSfxList Type

hi MatchParen           ctermbg=YELLOW     ctermfg=NONE        cterm=BOLD      guifg=NONE      guibg=YELLOW       gui=BOLD
hi Error                ctermfg=209        ctermbg=NONE        cterm=NONE      gui=NONE        guifg=NONE         guibg=#ff875f

" vim
hi link vimFunction Identifier

hi! link yamlBlockMappingKey            Keyword
hi! link yamlKeyValueDelimiter          Keyword
hi! link yamlBlockCollectionItemStart   Keyword


let g:colors_name = "my-acme"
